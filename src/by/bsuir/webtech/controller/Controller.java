package by.bsuir.webtech.controller;

import by.bsuir.webtech.controller.command.CommandException;
import by.bsuir.webtech.controller.command.CommandHelper;
import by.bsuir.webtech.controller.command.Executable;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {

    public Controller() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String commandName = request.getParameter(ParameterName.COMMAND);
        Executable command = CommandHelper.getInstance().getCommand(commandName);

        String page = null;
        try {
            page = command.execute(request, response);

        } catch (CommandException e) {
            page = JspPageName.SERVER_ERROR;
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        if (null == dispatcher) {
            response.sendRedirect(JspPageName.SERVER_ERROR);
        } else {
            dispatcher.forward(request, response);
        }
    }
}
