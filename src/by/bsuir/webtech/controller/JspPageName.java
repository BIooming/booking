package by.bsuir.webtech.controller;

public class JspPageName {

    //Common pages
    public static final String SERVER_ERROR = "servererror.jsp";
    public static final String LOGIN_ERROR = "loginerror.jsp";
    public static final String REGISTER = "register.jsp";
    public static final String INDEX = "index.jsp";
    public static final String PARTNERSHIP = "partnership.jsp";
    public static final String CONTACTS = "contacts.jsp";
    public static final String NO_SUCH_COMMAND = "WEB-INF/jsp/common/nosuchcommand.jsp";//TODO


    //Customer pages
    public static final String CUSTOMER_MAIN = "WEB-INF/jsp/customer/main.jsp";
    public static final String VIEW_ROOM_LIST = "WEB-INF/jsp/customer/rooms.jsp";
    public static final String NEW_RESERVATION = "WEB-INF/jsp/customer/reservation.jsp";
    public static final String CONFIRMATION = "WEB-INF/jsp/customer/confirmation.jsp";
    public static final String CUSTOMER_ACCOUNT = "WEB-INF/jsp/customer/account.jsp";
    public static final String EDIT_RESERVATION = "WEB-INF/jsp/customer/editreservation.jsp";

    //Administrator pages
    public static final String MANAGER_MAIN = "WEB-INF/jsp/manager/main.jsp";
    public static final String MANAGER_ACCOUNT = "WEB-INF/jsp/manager/account.jsp";
    public static final String ROOM_LIST = "WEB-INF/jsp/manager/rooms.jsp";
    public static final String RESERVATION_LIST = "WEB-INF/jsp/manager/reservations.jsp";
    public static final String USERS = "WEB-INF/jsp/manager/users.jsp";
    public static final String CREATE_USER = "WEB-INF/jsp/manager/createuser.jsp";
    public static final String EDIT_ROOM = "WEB-INF/jsp/manager/editroom.jsp";
    public static final String CANCEL_RESERVATION = "WEB-INF/jsp/manager/cancelreservation.jsp";
    public static final String EDIT_USER = "WEB-INF/jsp/manager/edituser.jsp";

    private JspPageName() {
    }
}
