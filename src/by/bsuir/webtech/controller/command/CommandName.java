package by.bsuir.webtech.controller.command;

public enum CommandName {

    //Common commands
    GOTO_REGISTER, GOTO_MAIN, GOTO_ACCOUNT,
    LOGIN, NO_SUCH_COMMAND, SET_LOCALE, LOG_OUT, REGISTER,

    //Customer commands
    GOTO_NEWRESERVATION, GOTO_ROOMLIST, GOTO_EDITRESERVATION,

    //Manager commands
    GOTO_ROOMS, GOTO_USERS, GOTO_RESERVATION_LIST,
    GOTO_CREATE_USER, GOTO_EDIT_USER,

}
