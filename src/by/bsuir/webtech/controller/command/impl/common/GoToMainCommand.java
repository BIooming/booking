package by.bsuir.webtech.controller.command.impl.common;

import by.bsuir.webtech.controller.command.CommandException;
import by.bsuir.webtech.controller.command.Executable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoToMainCommand implements Executable {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return null;
    }
}
