package by.bsuir.webtech.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Executable {

    String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException;
}
