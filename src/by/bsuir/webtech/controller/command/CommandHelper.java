package by.bsuir.webtech.controller.command;

import by.bsuir.webtech.controller.command.impl.common.*;

import java.util.HashMap;
import java.util.Map;

public class CommandHelper {
    private static CommandHelper instance = new CommandHelper();

    private Map<CommandName, Executable> commands = new HashMap<>();

    public static CommandHelper getInstance() {
        return instance;
    }

    public Executable getCommand(String commandName) {
        CommandName name = CommandName.valueOf(commandName);
        Executable command;
        if (null != name) {
            command = commands.get(name);
        } else {
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }

        return command;
    }

    private CommandHelper() {
        //common commands
        commands.put(CommandName.LOGIN, new LoginCommand());
        commands.put(CommandName.LOG_OUT, new LogOutCommand());
        commands.put(CommandName.SET_LOCALE, new SetLocaleCommand());
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
        commands.put(CommandName.GOTO_ACCOUNT, new GoToAccountCommand());
        commands.put(CommandName.GOTO_REGISTER, new GoToRegisterCommand());
        commands.put(CommandName.GOTO_MAIN, new GoToMainCommand());
    }
}
