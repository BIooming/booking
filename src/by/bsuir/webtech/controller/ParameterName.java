package by.bsuir.webtech.controller;

public class ParameterName {
    public static final String LOCALE = "locale";
    public static final String COMMAND = "command";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USER = "user";
    public static final String ADDRESS = "address";

    public static final String ROOM_LIST = "rooms";
    public static final String ROOM_TYPE_LIST = "roomTypes";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String DESCRIPTION = "description";

    private ParameterName() {
    }
}
