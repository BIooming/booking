package by.bsuir.webtech.dal;

import by.bsuir.webtech.bean.RoomType;

import java.util.HashMap;

public interface RoomTypeDao {

    RoomType searchByRoomId(int id) throws DalException;

    HashMap<Integer, RoomType> getAll() throws DalException;

    boolean add(int id, RoomType roomType) throws DalException;

    boolean delete(int id) throws DalException;

}
