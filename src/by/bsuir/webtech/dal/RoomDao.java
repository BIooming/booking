package by.bsuir.webtech.dal;

import by.bsuir.webtech.bean.Room;
import by.bsuir.webtech.bean.RoomType;

import java.util.ArrayList;

public interface RoomDao {

    Room getById(int id) throws DalException;

    ArrayList<Room> getAll() throws DalException;

    ArrayList<Room> getByRoomType(RoomType roomType) throws DalException;

    ArrayList<Room> getByStatus(String status) throws DalException;
}
