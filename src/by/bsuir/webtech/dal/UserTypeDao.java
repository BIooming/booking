package by.bsuir.webtech.dal;

import by.bsuir.webtech.bean.User;
import by.bsuir.webtech.bean.UserType;

import java.util.ArrayList;
import java.util.HashMap;

public interface UserTypeDao {

    UserType searchByUserId(int userId) throws  DalException;

    ArrayList<Integer> searchUserIdByType(UserType userType) throws DalException;

    HashMap<Integer, UserType> getAll() throws DalException;

    boolean add(int userId, UserType userType) throws DalException;

    boolean delete(int userId) throws DalException;
}
