package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.bean.RoomType;
import by.bsuir.webtech.dal.DalException;
import by.bsuir.webtech.dal.RoomTypeDao;

import java.util.HashMap;

public class MySqlRoomTypeDao implements RoomTypeDao {

    private static MySqlRoomTypeDao instance = new MySqlRoomTypeDao();

    public static MySqlRoomTypeDao getInstance() {
        return instance;
    }

    @Override
    public RoomType searchByRoomId(int id) throws DalException {
        return null;
    }

    @Override
    public HashMap<Integer, RoomType> getAll() throws DalException {
        return null;
    }

    @Override
    public boolean add(int id, RoomType roomType) throws DalException {
        return false;
    }

    @Override
    public boolean delete(int id) throws DalException {
        return false;
    }

    private MySqlRoomTypeDao() {
    }
}
