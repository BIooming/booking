package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.dal.HostedAtDao;

public class MySqlHostedAtDao implements HostedAtDao {
    private static MySqlHostedAtDao instance = new MySqlHostedAtDao();

    public static MySqlHostedAtDao getInstance() {
        return instance;
    }

    private MySqlHostedAtDao() {
    }
}
