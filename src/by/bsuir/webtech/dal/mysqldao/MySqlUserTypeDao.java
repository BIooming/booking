package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.bean.UserType;
import by.bsuir.webtech.dal.DalException;
import by.bsuir.webtech.dal.UserTypeDao;

import java.util.ArrayList;
import java.util.HashMap;

public class MySqlUserTypeDao implements UserTypeDao {

    private static MySqlUserTypeDao instance = new MySqlUserTypeDao();

    public static MySqlUserTypeDao getInstance() {
        return instance;
    }

    @Override
    public UserType searchByUserId(int userId) throws DalException {
        return null;
    }

    @Override
    public ArrayList<Integer> searchUserIdByType(UserType userType) throws DalException {
        return null;
    }

    @Override
    public HashMap<Integer, UserType> getAll() throws DalException {
        return null;
    }

    @Override
    public boolean add(int userId, UserType userType) throws DalException {
        return false;
    }

    @Override
    public boolean delete(int userId) throws DalException {
        return false;
    }

    private MySqlUserTypeDao() {
    }
}
