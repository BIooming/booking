package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.dal.ReservedRoomDao;

public class MySqlReservedRoomDao implements ReservedRoomDao {
    private static MySqlReservedRoomDao instance = new MySqlReservedRoomDao();

    public static MySqlReservedRoomDao getInstance() {
        return instance;
    }

    private MySqlReservedRoomDao() {
    }
}
