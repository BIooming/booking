package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.bean.Room;
import by.bsuir.webtech.bean.RoomType;
import by.bsuir.webtech.dal.DalException;
import by.bsuir.webtech.dal.RoomDao;

import java.util.ArrayList;

public class MySqlRoomDao implements RoomDao {

    private static MySqlRoomDao instance = new MySqlRoomDao();

    public static MySqlRoomDao getInstance() {
        return instance;
    }

    @Override
    public Room getById(int id) throws DalException {
        return null;
    }

    @Override
    public ArrayList<Room> getAll() throws DalException {
        return null;
    }

    @Override
    public ArrayList<Room> getByRoomType(RoomType roomType) throws DalException {
        return null;
    }

    @Override
    public ArrayList<Room> getByStatus(String status) throws DalException {
        return null;
    }

    private MySqlRoomDao() {
    }
}
