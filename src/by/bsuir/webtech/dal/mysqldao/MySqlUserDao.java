package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.bean.User;
import by.bsuir.webtech.dal.DalException;
import by.bsuir.webtech.dal.UserDao;

import java.util.ArrayList;

public class MySqlUserDao implements UserDao {

    private static MySqlUserDao instance = new MySqlUserDao();

    public static MySqlUserDao getInstance() {
        return instance;
    }

    @Override
    public User searchByID(int id) throws DalException {
        return null;
    }

    @Override
    public User searchByFirstName(String firsName) throws DalException {
        return null;
    }

    @Override
    public User searchByLastName(String lastName) throws DalException {
        return null;
    }

    @Override
    public User searchByLogin(String login) throws DalException {
        return null;
    }

    @Override
    public ArrayList<User> getAll() throws DalException {
        return null;
    }

    @Override
    public boolean update(User user) throws DalException {
        return false;
    }

    @Override
    public boolean deleteById(int id) throws DalException {
        return false;
    }

    private MySqlUserDao() {
    }
}
