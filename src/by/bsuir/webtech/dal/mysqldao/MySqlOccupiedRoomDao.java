package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.dal.OccupiedRoomDao;

public class MySqlOccupiedRoomDao implements OccupiedRoomDao {
    private static MySqlOccupiedRoomDao instance = new MySqlOccupiedRoomDao();

    public static MySqlOccupiedRoomDao getInstance() {
        return instance;
    }

    private MySqlOccupiedRoomDao() {
    }
}
