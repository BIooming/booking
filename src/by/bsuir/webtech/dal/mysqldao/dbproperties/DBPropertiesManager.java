package by.bsuir.webtech.dal.mysqldao.dbproperties;

import java.util.ResourceBundle;

/**
 * Created by blooming on 9.2.16.
 */
public final class DBPropertiesManager {
    private static final DBPropertiesManager instance = new DBPropertiesManager();
    private ResourceBundle bundle = ResourceBundle.getBundle(DBProperties.DB_CONFIG_FILE);

    private DBPropertiesManager() {
    }

    public static DBPropertiesManager getInstance() {
        return instance;
    }

    public String getProperty(String key) {
        return bundle.getString(key);
    }
}
