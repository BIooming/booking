package by.bsuir.webtech.dal.mysqldao;

import by.bsuir.webtech.bean.Reservation;
import by.bsuir.webtech.dal.DalException;
import by.bsuir.webtech.dal.ReservationDao;

import java.util.ArrayList;

public class MySqlReservationDao implements ReservationDao {

    private static MySqlReservationDao instance = new MySqlReservationDao();

    public static MySqlReservationDao getInstance() {
        return instance;
    }

    @Override
    public Reservation getById(int id) throws DalException {
        return null;
    }

    @Override
    public ArrayList<Reservation> getByUserId(int userId) throws DalException {
        return null;
    }

    @Override
    public boolean add(Reservation reservation) throws DalException {
        return false;
    }

    @Override
    public boolean delete(Reservation reservation) throws DalException {
        return false;
    }

    @Override
    public boolean update(Reservation reservation) throws DalException {
        return false;
    }

    private MySqlReservationDao() {
    }
}
