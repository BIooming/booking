package by.bsuir.webtech.dal.mysqldao.connectionpool;

public class ConnectionPoolException extends Exception {

    private static final long SerialVersionUID = 1L;

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Exception e) {
        super(message, e);
    }
}
