package by.bsuir.webtech.dal;

import by.bsuir.webtech.bean.UserType;
import by.bsuir.webtech.dal.factoryimpl.MySqlDaoFactory;

public abstract class DaoFactory {

    public static DaoFactory getDaoFactory() {
        return MySqlDaoFactory.getInstance();
    }

    public abstract HostedAtDao getHostedAtDao();

    public abstract OccupiedRoomDao getOccupiedRoomDao();

    public abstract ReservationDao getReservationDao();

    public abstract ReservedRoomDao getReservedRoomDao();

    public abstract RoomDao getRoomDao();

    public abstract RoomTypeDao getRoomTypeDao();

    public abstract UserDao getUserDao();

    public abstract UserTypeDao getUserTypeDao();

}
