package by.bsuir.webtech.dal;

import by.bsuir.webtech.bean.User;

import java.util.ArrayList;

public interface UserDao {

    User searchByID(int id) throws DalException;

    User searchByFirstName(String firsName) throws DalException;

    User searchByLastName(String lastName) throws DalException;

    User searchByLogin(String login) throws DalException;

    ArrayList<User> getAll() throws DalException;

    boolean update(User user) throws DalException;

    boolean deleteById(int id) throws DalException;
}
