package by.bsuir.webtech.dal.factoryimpl;

import by.bsuir.webtech.dal.*;
import by.bsuir.webtech.dal.mysqldao.*;

public class MySqlDaoFactory extends DaoFactory {
    private static MySqlDaoFactory instance = new MySqlDaoFactory();

    public static MySqlDaoFactory getInstance() {
        return instance;
    }

    private MySqlDaoFactory() {
    }

    @Override
    public HostedAtDao getHostedAtDao() {
        return MySqlHostedAtDao.getInstance();
    }

    @Override
    public OccupiedRoomDao getOccupiedRoomDao() {
        return MySqlOccupiedRoomDao.getInstance();
    }

    @Override
    public ReservationDao getReservationDao() {
        return MySqlReservationDao.getInstance();
    }

    @Override
    public ReservedRoomDao getReservedRoomDao() {
        return MySqlReservedRoomDao.getInstance();
    }

    @Override
    public RoomDao getRoomDao() {
        return MySqlRoomDao.getInstance();
    }

    @Override
    public RoomTypeDao getRoomTypeDao() {
        return MySqlRoomTypeDao.getInstance();
    }

    @Override
    public UserDao getUserDao() {
        return MySqlUserDao.getInstance();
    }

    @Override
    public UserTypeDao getUserTypeDao() {
        return MySqlUserTypeDao.getInstance();
    }
}
