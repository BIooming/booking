package by.bsuir.webtech.dal;

import by.bsuir.webtech.bean.Reservation;

import java.util.ArrayList;

public interface ReservationDao {

    Reservation getById(int id) throws DalException;

    ArrayList<Reservation> getByUserId(int userId) throws DalException;

    boolean add(Reservation reservation) throws DalException;

    boolean delete(Reservation reservation) throws DalException;

    boolean update(Reservation reservation) throws DalException;
}
