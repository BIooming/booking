package by.bsuir.webtech.service;

import by.bsuir.webtech.bean.User;
import by.bsuir.webtech.dal.DalException;
import by.bsuir.webtech.dal.DaoFactory;
import by.bsuir.webtech.dal.UserDao;
import by.bsuir.webtech.dal.UserTypeDao;

public class LoginService {
    private static final DaoFactory daoFactory = DaoFactory.getDaoFactory();
    private static final UserDao userDao = daoFactory.getUserDao();
    private static final UserTypeDao userTypeDao = daoFactory.getUserTypeDao();

    private static final LoginService instance = new LoginService();

    private LoginService() {
    }

    public static LoginService getInstance() {
        return instance;
    }

    public User login(String login, String password) throws ServiceException {
        User user = null;
        try {
            user = userDao.searchByLogin(login);
            if (isValid(user, password)) {
                user.setUserType(userTypeDao.searchByUserId(user.getId()));
            } else {
                user = null;
            }
        } catch (DalException e) {
            throw new ServiceException();
        }
        return user;
    }

    private boolean isValid(User user, String password) throws ServiceException {
        boolean isValid = true;
        if (user.getId() < 0) {
            isValid = false;
        }
        if (isWrongPassword(user, password)) {
            isValid = false;
        }
        return isValid;
    }


    private boolean isWrongPassword(User user, String password) throws ServiceException {
        boolean isWrong = true;
        if (password.equals(user.getPassword())) {
            isWrong = false;
        }
        return isWrong;
    }
}
