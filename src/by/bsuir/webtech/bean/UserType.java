package by.bsuir.webtech.bean;

public enum UserType {
    GUEST, ADMINISTRATOR
}
